# Cities info
Комплект программ для получения информации о выбранном городе.  
#
## Описание:
Это практическая работа для тренировки написания разноцелевых программ, организации системы взаимодействия между ними и работы с API-запросами.  
В комплекте реализованы три программы:  

### russian_cities.py
Основная программа для выбора города и получения:   
= данных из архива russuan_cities.json  
= информации о погоде с сайта openweather.map через программу weather.py  
= статьи из википедии с сайта wikipedia.org через программу wiki.py  

Программа распознаёт название города, даже если оно введено не полностью или введено с ошибкой.  
При нескольких подходящих вариантах, предлагает пользователю выбрать нужный из списка или повторить ввод.  
  
### weather.py
Второстепенная программа для получения информации о погоде с сайта openweathermap.org и вывода в читаемом виде.  
Для API-запроса на сайт, нужен словарь параметров. Если он не поступает с данными, то используются параметры из params.json.  
При запуске основного меню, можно менять настройки параметров.  
Так же программа может работать независимо от основной.  

### wiki.py
Второстепенная программа для получения информации с сайта wikipedia.org и вывода в читаемом виде.  
Информация со страницы википедии обрабатывается по главам.  
Текст каждой главы обработан для удобного чтения (перенос по строкам после каждой точки или при достижении 120 символов в строке).  
Пользователь может получить весь текст или выбрать для вывода только определённую главу.  
Так же программа может работать независимо от основной.  

### Модули:
- input.py  
Весь ввод информации от пользователя во всех программах реализован через функцию ask_user в модуле input.py в пакете modules.  
Ввод отдельно распознаёт команду 'q' и завершает работу программы. Благодаря этому программу можно завершить из любого этапа.  
Когда от пользователя требуется ввод числа(для выбора действия), запрос на ввод будет повторяться до тех пор, пока не будет соответствовать доступному в текущий момент диапазону (или 'q').  

- open.py  
Всё взаимодействие с json-файлами реализовано через функцию open_json в модуле open.py в пакете modules.  

### Файлы:  
- params.json  
Словарь параметров для запроса на сайт openweathermap.org.  

- russian_cities.json  
Словарь городов России со значениями словарями данных о соответствующем городе.  
#
## Использование:
- Запустите russian_cities.py  
- Введите предполагаемое название города. Если под запрос подходит несколько вариантов, выберете нужный или повторите ввод.  
- Выберете тип информации, который хотели-бы получить.  