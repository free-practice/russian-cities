"""Программа для получения и вывода информации с сайта wikipedia.org"""
import wikipedia

from modules.input import ask_user
from modules.open import open_json


def main(**kwargs):
    """Основное меню выбора и вывода информации."""
    name = kwargs.get('name') or ask_user(text='Введите запрос для википедии: ')
    wiki_data = get_wiki_dict(name)
    chapters = [k for k, v in wiki_data.items() if v]
    while True:
        print(f'\nО запросе {name} есть данные:'
              '\n(1) Вывести всё.'
              )
        # Выводим нумерованный список глав статьи.
        for num, chapter in enumerate(chapters, 2):
            print(f"({num}) {chapter.replace('=', '').strip()}.")
        print('(0) Отмена.')
        var = ask_user(span=(0, len(chapters)+2))
        # Если ввод == 0, завершаем цикл.
        if var == 0:
            break
        # Если ввод == 1, выводим всю статью.
        elif var == 1:
            for k, v in wiki_data.items():
                print(f'\n{k}\n{v}')
        # Если ввод == номер главы, выводим главу.
        else:
            print('', chapters[var-2], wiki_data[chapters[var-2]], sep='\n')
        input('Нажмите Enter, чтобы продолжить...')


def get_wiki_content(name):
    """Вернёт текст запрошенной страницы в формате str.
    В случае ошибки - выведет тип ошибки с описанием и вернёт None.
    Обязательный аргумент: name - текст запроса.
    """
    # Если в архиве russian_cities.json есть корректировка 'wiki_region', добвляем её к запросу.
    if correction := open_json('russian_cities.json')[name].get('wiki_region'):
        name = f'{name} ({correction})'
    try:
        wikipedia.set_lang('ru')
        return wikipedia.page(name).content
    except Exception as error:
        print(f"Ошибка: {type(error)}\n{error}")


def get_wiki_dict(name):
    """Вернёт обработанный текст запрошенной страницы в формате dict. Обязательный аргумент: name - текст запроса."""
    wiki_list = get_wiki_content(name).split()
    text = ''
    head = '== Основная информация =='
    wiki_data = {head: ''}

    while wiki_list:
        word = wiki_list.pop(0)

        # Отдельно распознаём заголовки и подзаголовки. Для заголовков создаём новый ключ.
        if word.startswith('=='):
            title = word
            while True:
                word = wiki_list.pop(0)
                title += ' ' + word
                if word.startswith('=='):
                    break
            if title.startswith('== '):
                head = title
                wiki_data[head] = ''
            else:
                wiki_data[head] += '\n' + title + '\n'
            continue
        
        # Проверяем чтобы предложение не превышало длины в 120 символов.
        if (len(word) + len(text) + 1) <= 120:
            # Если предложение закончилось точкой - вставляем перенос строки. Отсеиваем сокращения.
            if word[-1] == '.' and len(word) > 3:
                wiki_data[head] += text + word + '\n'
                text = ''
            else:
                text += word + ' '

        # Вставляем перенос строки, если количество символов превысит 120.
        else:
            wiki_data[head] += text + '\n' + word + ' '
            text = ''

    return wiki_data


if __name__ == '__main__':
    while True:
        print("Для завершения программы в любой момент введите 'q'.")
        main()
