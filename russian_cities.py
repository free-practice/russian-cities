"""Программа для получения и вывода информации о городах России."""
import re

from modules.input import ask_user
from modules.open import open_json
from weather import main as weather, weather_print
from wiki import main as wiki


def main():
    """Основное меню определения города и получения информации о нём."""
    cities = open_json('russian_cities.json')

    # Запрашиваем ввод названия города и запускаем цикл проверки и корректировки.
    while city := ask_user(text='Введите предполагаемое название города: ').capitalize():
        if city := change_city(city, cities):
            break

    # Запрашиваем и реализуем выбор действия с городом.
    while True:
        print(f'\nВыбранный город: {city}'
              '\n(1) Получить данные из архива.'
              '\n(2) Узнать погоду в городе.'
              '\n(3) Получить информацию из википедии.'
              '\n(0) Отмена.'
              )
        var = ask_user(span=(0, 4))
        match var:
            case 1:
                # Выведет все данные, что есть в архиве на этот город.
                for k, v in sorted(cities[city].items()):
                    print(f'{k}: {v}')
                input('Нажмите Enter чтобы продолжить...')
            case 2:
                params = open_json('params.json')
                params['q'] = city
                # Если weather_print не вывел погоду, то запускается меню настройки параметров.
                if not weather_print(params=params):
                    weather(params=params)
            case 3:
                wiki(name=city)
            case 0:
                break


def change_city(city, cities):
    """Вернёт корректное название города или False, при выборе повторного ввода.
    Обязательные позиционные аргументы:
    city - предполагаемое название (или часть названия) города.
    cities - словарь или список городов России.
    """
    cities_str = '\n' + '\n'.join(cities) + '\n'
    city_list = list(city)
    offer = set()

    # Задаём условие е == ё и если первая часть названия города введена верно - составляем список подходящих имен.
    while 'е' in city_list:
        city_list[city_list.index('е')] = '[её]'
    city_ = ''.join(city_list)
    offer = offer | set(re.findall(fr'\n({city_}.*)\n', cities_str, flags=re.IGNORECASE))
    # При единственном подобранном варианте, сразу возвращаем результат.
    if len(offer) == 1:
        return list(offer)[0]

    # Если нет совпадений, то составляем список offer с проверкой на возможную опечатку.
    if not offer and len(city) > 2:
        for i, j in enumerate(city_list):
            city_list_ = city_list[::]
            city_list_[i] = f'{j}?[а-я]?{j}?'
            city_ = ''.join(city_list_)
            offer = offer | set(re.findall(fr'\n({city_}.*)\n', cities_str, flags=re.IGNORECASE))
        # При единственном подобранном варианте, сразу возвращаем результат.
        if len(offer) == 1:
            return list(offer)[0]

    # Предлагаем пользователю выбрать вариант из списка совпадений(если они есть) или ввести название заново.
    offer = sorted(offer)
    print('Возможно вы имели ввиду:',
          *[f'\n({i}) {j}' for i, j in enumerate(offer, 1)] if offer else ["Нет совпадений."]
          )
    var = ask_user(span=(0, len(offer)+1), text='Введите число города или 0 для ввода заново: ') if offer else 0
    if var:
        return offer[var-1]
    return False


if __name__ == '__main__':
    while True:
        print("Для завершения программы в любой момент введите 'q'.")
        main()
