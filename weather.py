"""Программа для получения и вывода информации о погоде с сайта openweathermap.org"""
import requests

from modules.input import ask_user
from modules.open import open_json


def main(**kwargs):
    """Основное меню выбора для вывода погоды и изменения параметров.
    Необязательный аргумент: params - параметры для запроса на сайт openweathermap.org.
    """
    # Если параметры не поступили, загружаем их из файла.
    params = kwargs.get('params') or open_json('params.json')

    # Цикл завершается только после вывода информации о погоде или при вводе 0.
    while True:
        print("\n(1) Получить данные о погоде."
              f"\n(2) Изменить токен. Текущий: {params['appid']}."
              f"\n(3) Изменить город. Текущий: {params['q']}."
              "\n(4) Сбросить параметры: (Город - Москва, удалить токен)."
              "\n(0) Отмена."
              )
        var = ask_user(span=(0, 5))
        match var:
            case 0 | 1:
                if {0: True, 1: weather_print(params=params)}[var]:
                    break
                continue
            case 2:
                params['appid'] = ask_user(text='Введите токен: ')
            case 3:
                params['q'] = ask_user(text='Введите название города: ')
            case 4:
                if ask_user(span=(0, 2), text='Токен будет удалён!\n(0)Отмена\n(1)Продолжить\nСбросить параметры?: '):
                    params = {'q': 'Москва', 'appid': 0, 'units': 'metric'}
        open_json('params.json', mode='w', params=params)


def weather_api(**kwargs):
    """В случае успеха - вернёт словарь данных о погоде.
    В случае ошибки ответа сервера - печатает тип ошибки и возвращает None.
    Необязательный аргумент: params - параметры для запроса на сайт openweathermap.org.
    """
    # Если параметры не поступили, загружаем их из файла.
    params = kwargs.get('params') or open_json('params.json')
    response = requests.get('https://api.openweathermap.org/data/2.5/weather', params=params)
    if response:
        weather_data = response.json()
        return weather_data

    error = response.status_code
    errors = {401: 'Возможно токен неверный.', 404: 'Возможно город задан неверно.'}
    print(f"Ошибка {error}: {errors.get(error, 'Неизвестная ошибка.')}")


def weather_print(**kwargs):
    """В случае успеха - выведет данные о погоде в читаемом виде и вернёт True.
    В случае ошибки обработки данных - печатает тип ошибки и возвращает None.
    Необязательный аргумент: params - параметры для запроса на сайт openweathermap.org.
    """
    # Если параметры не поступили, загружаем их из файла.
    params = kwargs.get('params') or open_json('params.json')

    if weather_data := weather_api(params=params):
        directions = ("северный", "северо-восточный", "восточный", "юго-восточный", "южный", "юго-западный",
                      "западный", "северо-западный")
        try:
            sky = (weather_data['weather'][0]['main'], weather_data['weather'][0]['description'])
            temp = (weather_data['main']['temp'], weather_data['main']['feels_like'])
            wind = (weather_data['wind']['speed'], weather_data['wind']['gust'])
            direction = directions[int((weather_data['wind']['deg'] + 22.5) // 45 % 8)]
            air = (weather_data['main']['pressure'], weather_data['main']['humidity'])

            print(f"\nСейчас в городе {params['q']}: {sky[0]} - {sky[1]},\n"
                  f"Температура {temp[0]}°, ощущается как {temp[1]}°,\n"
                  f"Ветер {direction}, {wind[0]}м/с, с порывами до {wind[1]}м/с.,\n"
                  f"Давление {int(air[0] / 1.33322)}мм рт/ст, влажность {air[1]}%."
                  )
            input('Нажмите Enter чтобы продолжить...')
            return True

        except Exception as error:
            print(f'Ошибка в {error}, {type(error)}.')


if __name__ == '__main__':
    while True:
        print("Для завершения программы в любой момент введите 'q'.")
        main()
