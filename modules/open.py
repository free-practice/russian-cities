"""Модуль для работы с файлами формата json."""
import json


def open_json(name, encoding='utf-8', ensure_ascii=False, mode='r', *args, **kwargs):
    """Вернёт словарь данных из json-файла. При mode='w' запишет параметры(params) в json-файл.
    Обязательный аргумент: name - имя открываемого json-файла.
    Необязательные аргументы:
    encoding - кодировка, используемая при открытии файла (по умолчанию 'utf-8').
    ensure_ascii - экранирование не-ASCII символов при записи в файл (по умолчанию False).
    mode - режим открытия файла (по умолчанию 'r').
    params - архив параметров для перезаписи в json-файл при mode='w'.
    """
    with open(name, mode=mode, encoding=encoding) as file:
        match mode:
            case 'r' | 'r+':
                data = json.load(file)
                return data
            case 'w' | 'w+':
                if 'params' in kwargs:
                    json.dump(kwargs['params'], file, ensure_ascii=ensure_ascii)
