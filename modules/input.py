"""Модуль для получения ввода от пользователя."""
import sys


def ask_user(span=(), text='Введите значение: '):
    """Вернёт ввод от пользователя или завершит программу, если ввод равен q.
    Необязательные аргументы:
    span - диапазон, которому должно соответствовать число от пользователя.
    text - выводимый текст, при запросе на ввод.
    """
    while True:
        ask = input('\n' + text)
        if ask.lower() == 'q':
            sys.exit()
        if span:
            if ask.isdigit() and int(ask) in range(*span):
                return int(ask)
            continue
        return ask
